import logo from "./logo.svg";
import "./App.css";
import Header from "./Baitapthuchanhlayout/Header";
import Banner from "./Baitapthuchanhlayout/Banner";
import Item from "./Baitapthuchanhlayout/Item";
import Footer from "./Baitapthuchanhlayout/Footer";

function App() {
  return (
    <div className="App">
      <Header />
      <Banner />
      <Item />
      <Footer />
    </div>
  );
}

export default App;
